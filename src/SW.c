#include "SWDefinitions.h"
#include "SWWatchface.h"


SWWATCHFACE SWWatchface;

static Window* window;

/*static Layer time_layer;
static Layer date_layer;
static Layer line_layer;
static Layer day_layer;
*/

static void handle_minute_tick(struct tm* t , TimeUnits units_changed) 
{
	GetSetCurrentTime(&SWWatchface.SWTime, bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
	if (t->tm_mday != SWWatchface.SWDate.iCurrentDay)
	{
		SetCurrentDateAndDateWord(&SWWatchface.SWDate, bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
	}
}

void window_load()
{
	if (!SWWatchface.State.bClosing)
	{
		time_t t = time(NULL);
		struct tm* tick_time = localtime(&t);
		SetupWatchface(tick_time, window); 
	}
}

void handle_init(void) 
{
  window = window_create();
  window_set_background_color(window, GColorBlack);
  //window_set_fullscreen(window, true);
  
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    //.unload = window_unload,
  });
 
  tick_timer_service_subscribe(MINUTE_UNIT, handle_minute_tick);
  window_stack_push(window, true /* Animated */);
  
}

void handle_deinit(void) 
{
	RemoveAndDeInt(ALL);
	SWWATCHFACE_DEINIT(ALL);
	
	window_destroy(window);
}

int main(void)
{
  handle_init();
  app_event_loop();
  handle_deinit();
}

