//SW = Sideways;Watchface
#include "SWDefinitions.h"
#include "SWDate.h"
#include "SWTime.h"

typedef struct
{
	bool bExists;
	bool bInitializing;
	bool bInitialized;
	bool bSetBackground;
	bool bClosing;
	//static bool bLocated = false;
} WatchfaceStateVars;
	
//put in weather
//static int iOurLatitude, iOurLongitude;

typedef struct
{
	BitmapLayer*	lyrSWWatchface;
	GBitmap* imgBackground;
	GRect fraBackground;	

	WatchfaceStateVars State;	

	SWTIME		SWTime;
	SWDATE 	SWDate;
}SWWATCHFACE;
	
void CreateBackground();
void SetupWatchface(struct tm* t ,Window* window);
void RemoveAndDeInt(char* cToRnD);
void SWWATCHFACE_DEINIT(char* cToRnD);
//void RemoveAndDeIntAll(P4GWATCHFACE* P4GWatchface);
