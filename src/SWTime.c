#include "SWTime.h"

#define NUMBER_OF_IMAGES 11

static const int TIME_IMAGES[NUMBER_OF_IMAGES] = {
  RESOURCE_ID_IMAGE_BIG_0,
  RESOURCE_ID_IMAGE_BIG_1,
  RESOURCE_ID_IMAGE_BIG_2,
  RESOURCE_ID_IMAGE_BIG_3,
  RESOURCE_ID_IMAGE_BIG_4,
  RESOURCE_ID_IMAGE_BIG_5,
  RESOURCE_ID_IMAGE_BIG_6,
  RESOURCE_ID_IMAGE_BIG_7,
  RESOURCE_ID_IMAGE_BIG_8,
  RESOURCE_ID_IMAGE_BIG_9,
  RESOURCE_ID_IMAGE_BIG_10
};

void SetTimeImage(SWTIME* SWTime, Layer* layer)
{	
	if (SWTime->State.bInitialized)
	{
		if (SWTime->State.bUpdatePTDigit)
		{
			if (!SWTime->State.bExistsPTImg && !SWTime->State.bSetPTImg && 
				!SWTime->State.bExistsPTLyr && !SWTime->State.bSetPTLyr)
			{
				SWTime->imgTime[0] = gbitmap_create_with_resource(TIME_IMAGES[SWTime->iCurrentPastTwelveDigit]);
				SWTime->State.bExistsPTImg = true;
				SWTime->bmplyrTimePastTwelve = bitmap_layer_create(SWTime->fraTimePastTwelveFrame);
				SWTime->State.bExistsPTLyr = true;
				bitmap_layer_set_bitmap(SWTime->bmplyrTimePastTwelve, SWTime->imgTime[0]); //or .layer?
				SWTime->State.bSetPTImg = true;
				bitmap_layer_set_background_color(SWTime->bmplyrTimePastTwelve,GColorClear);
				bitmap_layer_set_compositing_mode(SWTime->bmplyrTimePastTwelve, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimePastTwelve));
				SWTime->State.bSetPTLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimePastTwelve));
			}
		}
		if (SWTime->State.bUpdateDot)
		{
			if (!SWTime->State.bExistsDotImg && !SWTime->State.bSetDotImg && 
				!SWTime->State.bExistsDotLyr && !SWTime->State.bSetDotLyr)
			{
				SWTime->imgTime[1] = gbitmap_create_with_resource(TIME_IMAGES[10]);
				SWTime->State.bExistsDotImg = true;
				SWTime->bmplyrTimeDot = bitmap_layer_create(SWTime->fraTimeDotFrame);
				SWTime->State.bExistsDotLyr = true;
				bitmap_layer_set_bitmap(SWTime->bmplyrTimeDot, SWTime->imgTime[1]); //or .layer?
				SWTime->State.bSetDotImg = true;
				bitmap_layer_set_background_color(SWTime->bmplyrTimeDot,GColorClear);
				bitmap_layer_set_compositing_mode(SWTime->bmplyrTimeDot, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimeDot));
				SWTime->State.bSetDotLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimeDot));
			}
		}
		//Black 1
		if (SWTime->State.bUpdateHTDigit)
		{
			if (!SWTime->State.bExistsHTImg && !SWTime->State.bSetHTImg && 
				!SWTime->State.bExistsHTLyr && !SWTime->State.bSetHTLyr)
			{
				SWTime->imgTime[2] = gbitmap_create_with_resource(TIME_IMAGES[SWTime->iCurrentHourTensDigit]);
				SWTime->State.bExistsHTImg = true;
				SWTime->bmplyrTimeHourTens = bitmap_layer_create(SWTime->fraTimeHourTensFrame);
				SWTime->State.bExistsHTLyr = true;
				bitmap_layer_set_bitmap(SWTime->bmplyrTimeHourTens, SWTime->imgTime[2]); //or .layer?
				SWTime->State.bSetHTImg = true;
				bitmap_layer_set_background_color(SWTime->bmplyrTimeHourTens,GColorClear);
				bitmap_layer_set_compositing_mode(SWTime->bmplyrTimeHourTens, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimeHourTens));
				SWTime->State.bSetHTLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimeHourTens));
			}
		}
		//Black 2
		if (SWTime->State.bUpdateHODigit)
		{	
			if (!SWTime->State.bExistsHOImg && !SWTime->State.bSetHOImg && 
				!SWTime->State.bExistsHOLyr && !SWTime->State.bSetHOLyr)
			{
				SWTime->imgTime[3] = gbitmap_create_with_resource(TIME_IMAGES[SWTime->iCurrentHourOnesDigit]);
				SWTime->State.bExistsHOImg = true;
				SWTime->bmplyrTimeHourOnes = bitmap_layer_create(SWTime->fraTimeHourOnesFrame);
				SWTime->State.bExistsHOLyr = true;
				bitmap_layer_set_bitmap(SWTime->bmplyrTimeHourOnes, SWTime->imgTime[3]); //or .layer?
				SWTime->State.bSetHOImg = true;
				bitmap_layer_set_background_color(SWTime->bmplyrTimeHourOnes,GColorClear);
				bitmap_layer_set_compositing_mode(SWTime->bmplyrTimeHourOnes, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimeHourOnes));
				SWTime->State.bSetHOLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimeHourOnes));
			}
		}
		//Black 3
		if (SWTime->State.bUpdateMTDigit)
		{
			if (!SWTime->State.bExistsMTImg && !SWTime->State.bSetMTImg && 
				!SWTime->State.bExistsMTLyr && !SWTime->State.bSetMTLyr)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Image  BITMAP 3"); 
				SWTime->imgTime[4] = gbitmap_create_with_resource(TIME_IMAGES[SWTime->iCurrentMinTensDigit]);
				SWTime->State.bExistsMTImg = true;
				SWTime->bmplyrTimeMinTens = bitmap_layer_create(SWTime->fraTimeMinTensFrame);
				SWTime->State.bExistsMTLyr = true;
				bitmap_layer_set_bitmap(SWTime->bmplyrTimeMinTens, SWTime->imgTime[4]); //or .layer?
				SWTime->State.bSetMTImg = true;
				bitmap_layer_set_background_color(SWTime->bmplyrTimeMinTens,GColorClear);
				bitmap_layer_set_compositing_mode(SWTime->bmplyrTimeMinTens, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimeMinTens));
				SWTime->State.bSetMTLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimeMinTens));
			}
		}
		//Black 4	
		if (SWTime->State.bUpdateMODigit)
		{
		  if (!SWTime->State.bExistsMOImg && !SWTime->State.bSetMOImg && 
			  !SWTime->State.bExistsMOLyr && !SWTime->State.bSetMOLyr)
		  {	
			  //APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Image  BITMAP 4"); 
			  SWTime->imgTime[5] = gbitmap_create_with_resource(TIME_IMAGES[SWTime->iCurrentMinOnesDigit]);
			  SWTime->State.bExistsMOImg = true;
			  SWTime->bmplyrTimeMinOnes = bitmap_layer_create(SWTime->fraTimeMinOnesFrame);
			  SWTime->State.bExistsMOLyr = true;
			  bitmap_layer_set_bitmap(SWTime->bmplyrTimeMinOnes, SWTime->imgTime[5]); //or .layer?
			  SWTime->State.bSetMOImg = true;
			  bitmap_layer_set_background_color(SWTime->bmplyrTimeMinOnes,GColorClear);
			  bitmap_layer_set_compositing_mode(SWTime->bmplyrTimeMinOnes, GCompOpAssign);
			  layer_add_child(layer, bitmap_layer_get_layer(SWTime->bmplyrTimeMinOnes));
			  SWTime->State.bSetMOLyr = true;
			  //SWTime->State.bSetMOImg = true;
			  layer_mark_dirty(bitmap_layer_get_layer(SWTime->bmplyrTimeMinOnes));	 
		  }
		}
	}
}
			
void SetTimeImages(SWTIME* SWTime, Layer* layer)
{		
	SetTimeImage(SWTime, layer);
	
	SWTime->State.bUpdatePTDigit = false;
	SWTime->State.bUpdateDot = false;
	SWTime->State.bUpdateHTDigit = false;
	SWTime->State.bUpdateHODigit = false;
	SWTime->State.bUpdateMTDigit = false;
	SWTime->State.bUpdateMODigit = false;
	//SWTime->iPreviousHour = SWTime->iCurrentHour;
}	

void RemoveAndDeIntChangingTime(SWTIME* SWTime)
{
	if (SWTime->State.bInitialized)
	{
		if (SWTime->State.bUpdatePTDigit)
		{			
			if (SWTime->State.bExistsPTLyr && SWTime->State.bSetPTLyr)
			{
				layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimePastTwelve));
				bitmap_layer_destroy(SWTime->bmplyrTimePastTwelve);
				SWTime->State.bSetPTLyr = false;
				SWTime->State.bExistsPTLyr = false;
			}
			
			if (SWTime->State.bSetPTImg  && SWTime->State.bExistsPTImg)
			{ 
				gbitmap_destroy(SWTime->imgTime[0]);
				SWTime->State.bSetPTImg = false;
				SWTime->State.bExistsPTImg = false;
			}
		}
		
		if (SWTime->State.bUpdateHTDigit)
		{			
			if (SWTime->State.bExistsHTLyr && SWTime->State.bSetHTLyr)
			{
				layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeHourTens));
				bitmap_layer_destroy(SWTime->bmplyrTimeHourTens);
				SWTime->State.bSetHTLyr = false;
				SWTime->State.bExistsHTLyr = false;
			}
			
			if (SWTime->State.bSetHTImg  && SWTime->State.bExistsHTImg)
			{ 
				gbitmap_destroy(SWTime->imgTime[2]);
				SWTime->State.bSetHTImg = false;
				SWTime->State.bExistsHTImg = false;
			}
		}
		
		if (SWTime->State.bUpdateHODigit)
		{			
			if (SWTime->State.bExistsHOLyr && SWTime->State.bSetHOLyr)
			{
				layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeHourOnes));
				bitmap_layer_destroy(SWTime->bmplyrTimeHourOnes);
				SWTime->State.bSetHOLyr = false;
				SWTime->State.bExistsHOLyr = false;
			}
			
			if (SWTime->State.bSetHOImg  && SWTime->State.bExistsHOImg)
			{ 
				gbitmap_destroy(SWTime->imgTime[3]);
				SWTime->State.bSetHOImg = false;
				SWTime->State.bExistsHOImg = false;
			}
		}
		
		if (SWTime->State.bUpdateMTDigit)
		{			
			if (SWTime->State.bExistsMTLyr && SWTime->State.bSetMTLyr)
			{
				layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeMinTens));
				bitmap_layer_destroy(SWTime->bmplyrTimeMinTens);
				SWTime->State.bSetMTLyr = false;
				SWTime->State.bExistsMTLyr = false;
			}
			
			if (SWTime->State.bSetMTImg  && SWTime->State.bExistsMTImg)
			{ 
				gbitmap_destroy(SWTime->imgTime[4]);
				SWTime->State.bSetMTImg = false;
				SWTime->State.bExistsMTImg = false;
			}
		}
		
		if (SWTime->State.bUpdateMODigit)
		{			
			if (SWTime->State.bExistsMOLyr && SWTime->State.bSetMOLyr)
			{
				layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeMinOnes));
				bitmap_layer_destroy(SWTime->bmplyrTimeMinOnes);
				SWTime->State.bSetMOLyr = false;
				SWTime->State.bExistsMOLyr = false;
			}
			
			if (SWTime->State.bSetMOImg  && SWTime->State.bExistsMOImg)
			{ 
				gbitmap_destroy(SWTime->imgTime[5]);
				SWTime->State.bSetMOImg = false;
				SWTime->State.bExistsMOImg = false;
			}
		}
	}
}

void RemoveAndDeIntTime(SWTIME* SWTime)
{
	if (SWTime->State.bInitialized)
	{		
		
		
		if (SWTime->State.bExistsPTLyr && SWTime->State.bSetPTLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimePastTwelve));
			bitmap_layer_destroy(SWTime->bmplyrTimePastTwelve);
			SWTime->State.bExistsPTLyr = false;
			SWTime->State.bSetPTLyr = false;
		}	
		
		if (SWTime->State.bSetPTImg  && SWTime->State.bExistsPTImg)
		{
			gbitmap_destroy(SWTime->imgTime[0]);
			SWTime->State.bSetPTImg = false;
			SWTime->State.bExistsPTImg = false;
		}
		
		if (SWTime->State.bExistsDotLyr && SWTime->State.bSetDotLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeDot));
			bitmap_layer_destroy(SWTime->bmplyrTimeDot);
			SWTime->State.bExistsDotLyr = false;
			SWTime->State.bSetDotLyr = false;
		}	
		
		if (SWTime->State.bSetDotImg  && SWTime->State.bExistsDotImg)
		{
			gbitmap_destroy(SWTime->imgTime[1]);
			SWTime->State.bSetDotImg = false;
			SWTime->State.bExistsDotImg = false;
		}
		
		if (SWTime->State.bExistsHTLyr && SWTime->State.bSetHTLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeHourTens));
			bitmap_layer_destroy(SWTime->bmplyrTimeHourTens);
			SWTime->State.bSetHTLyr = false;
			SWTime->State.bExistsHTLyr = false;
		}
		
		if (SWTime->State.bSetHTImg  && SWTime->State.bExistsHTImg)
		{ 
			gbitmap_destroy(SWTime->imgTime[2]);
			SWTime->State.bSetHTImg = false;
			SWTime->State.bExistsHTImg = false;
		}	
		
		if (SWTime->State.bExistsHOLyr && SWTime->State.bSetHOLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeHourOnes));
			bitmap_layer_destroy(SWTime->bmplyrTimeHourOnes);
			SWTime->State.bSetHOLyr = false;
			SWTime->State.bExistsHOLyr = false;
		}
		
		if (SWTime->State.bSetHOImg  && SWTime->State.bExistsHOImg)
		{ 
			gbitmap_destroy(SWTime->imgTime[3]);
			SWTime->State.bSetHOImg = false;
			SWTime->State.bExistsHOImg = false;
		}
		
		if (SWTime->State.bExistsMTLyr && SWTime->State.bSetMTLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeMinTens));
			bitmap_layer_destroy(SWTime->bmplyrTimeMinTens);
			SWTime->State.bSetMTLyr = false;
			SWTime->State.bExistsMTLyr = false;
		}
		
		if (SWTime->State.bSetMTImg  && SWTime->State.bExistsMTImg)
		{ 
			gbitmap_destroy(SWTime->imgTime[4]);
			SWTime->State.bSetMTImg = false;
			SWTime->State.bExistsMTImg = false;
		}

		if (SWTime->State.bExistsMOLyr && SWTime->State.bSetMOLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWTime->bmplyrTimeMinOnes));
			bitmap_layer_destroy(SWTime->bmplyrTimeMinOnes);
			SWTime->State.bSetMOLyr = false;
			SWTime->State.bExistsMOLyr = false;
		}

		if (SWTime->State.bSetMOImg  && SWTime->State.bExistsMOImg)
		{ 
			gbitmap_destroy(SWTime->imgTime[5]);
			SWTime->State.bSetMOImg = false;
			SWTime->State.bExistsMOImg = false;
		}		
	}
}

void SetCurrentTime(SWTIME* SWTime, Layer* layer)
{
	if (SWTime->State.bInitialized)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Remove and deint prev."); 
		RemoveAndDeIntChangingTime(SWTime);
		//SWTime->iCurrentHour = t->tm_hour;
		SetTimeImages(SWTime, layer);
	}
}

void GetCurrentTime(SWTIME* SWTime)
{
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	int hour = tmTime->tm_hour;
	SWTime->iCurrentHour = hour;	
	
	
	if (hour >=12)
	{
		SWTime->iCurrentPastTwelveDigit = 1;
	}
	else
	{
		SWTime->iCurrentPastTwelveDigit = 0;
	}
	
	if (!clock_is_24h_style()) 
	{
		hour = hour % 12;
		if (hour == 0) 
		{
			hour = 12;
		}	
	}
	
	int value = hour %100;
	
	
	SWTime->iCurrentHourTensDigit = value / 10;
	SWTime->iCurrentHourOnesDigit = value % 10;
	
	value = tmTime->tm_min %100;
	SWTime->iCurrentMinTensDigit = value / 10;
	SWTime->iCurrentMinOnesDigit = value % 10;
	
	if (SWTime->iPreviousPastTwelveDigit != SWTime->iCurrentPastTwelveDigit)
	{
		SWTime->State.bUpdatePTDigit = true;
	}
	else
	{
		SWTime->State.bUpdatePTDigit = false;
	}
	SWTime->iPreviousPastTwelveDigit = SWTime->iCurrentPastTwelveDigit;
	
	if (SWTime->iPreviousHourTensDigit != SWTime->iCurrentHourTensDigit)
	{
		SWTime->State.bUpdateHTDigit = true;
	}
	else
	{
		SWTime->State.bUpdateHTDigit = false;
	}
	SWTime->iPreviousHourTensDigit = SWTime->iCurrentHourTensDigit;
	
	if (SWTime->iPreviousHourOnesDigit != SWTime->iCurrentHourOnesDigit)
	{
		SWTime->State.bUpdateHODigit = true;
	}
	else
	{
		SWTime->State.bUpdateHODigit = false;
	}
	SWTime->iPreviousHourOnesDigit = SWTime->iCurrentHourOnesDigit;
	
	if (SWTime->iPreviousMinTensDigit != SWTime->iCurrentMinTensDigit)
	{
		SWTime->State.bUpdateMTDigit = true;
	}
	else
	{
		SWTime->State.bUpdateMTDigit = false;
	}
	SWTime->iPreviousMinTensDigit = SWTime->iCurrentMinTensDigit;
	
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "prev min digit: %d current min ones digit: %d",SWTime->iPreviousMinOnesDigit,SWTime->iCurrentMinOnesDigit); 
	if (SWTime->iPreviousMinOnesDigit != SWTime->iCurrentMinOnesDigit)
	{
		SWTime->State.bUpdateMODigit = true;
	}
	else
	{
		SWTime->State.bUpdateMODigit = false;
	}
	SWTime->iPreviousMinOnesDigit = SWTime->iCurrentMinOnesDigit;
	
}

void GetSetCurrentTime(SWTIME* SWTime, Layer* layer)
{
	GetCurrentTime(SWTime);
	SetCurrentTime(SWTime, layer);
}

void SWTIME_INIT(SWTIME* SWTime)
{
	if (!SWTime->State.bInitialized)
	{
		SWTime->fraTimeFrame = WATCH_FRA_TIME;
		//TIME_IMAGES
		SWTime->State.bInitialized = true;
		SWTime->fraTimePastTwelveFrame = WATCH_FRA_TIME_PAST_TWELVE;
		SWTime->fraTimeDotFrame = WATCH_FRA_TIME_DOT;
		SWTime->fraTimeHourTensFrame = WATCH_FRA_TIME_HOUR_TENS;
		SWTime->fraTimeHourOnesFrame = WATCH_FRA_TIME_HOUR_ONES;
		SWTime->fraTimeMinTensFrame = WATCH_FRA_TIME_MINUTE_TENS;
		SWTime->fraTimeMinOnesFrame = WATCH_FRA_TIME_MINUTE_ONES;
		
		SWTime->iCurrentPastTwelveDigit = 0;
		SWTime->iCurrentHourTensDigit = 99;
		SWTime->iCurrentHourOnesDigit = 99;
		SWTime->iCurrentMinTensDigit = 99;
		SWTime->iCurrentMinOnesDigit = 99;
		
		SWTime->iPreviousPastTwelveDigit = 88;
		SWTime->iPreviousHourTensDigit = 88;
		SWTime->iPreviousHourOnesDigit = 88;
		SWTime->iPreviousMinTensDigit = 88;
		SWTime->iPreviousMinOnesDigit = 88;
		
		SWTime->State.bUpdatePTDigit = true;
		SWTime->State.bUpdateDot = true;
		SWTime->State.bUpdateHTDigit = true;
		SWTime->State.bUpdateHODigit = true;
		SWTime->State.bUpdateMTDigit = true;
		SWTime->State.bUpdateMODigit = true;
	}
}

void SWTIME_DEINIT(SWTIME* SWTime)
{
	SWTime->State.bInitialized = false;	
}
