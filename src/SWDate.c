#include "SWDate.h"

#define NUMBER_OF_IMAGES 10
#define NUMBER_OF_DAY 7                                                

static const int DATE_IMAGES[NUMBER_OF_IMAGES] = {
  RESOURCE_ID_IMAGE_SMALL_0,
  RESOURCE_ID_IMAGE_SMALL_1,
  RESOURCE_ID_IMAGE_SMALL_2,
  RESOURCE_ID_IMAGE_SMALL_3,
  RESOURCE_ID_IMAGE_SMALL_4,
  RESOURCE_ID_IMAGE_SMALL_5,
  RESOURCE_ID_IMAGE_SMALL_6,
  RESOURCE_ID_IMAGE_SMALL_7,
  RESOURCE_ID_IMAGE_SMALL_8,
  RESOURCE_ID_IMAGE_SMALL_9
};

const int DATE_WORD_IMAGES[NUMBER_OF_DAY] = {
  RESOURCE_ID_IMAGE_DAY_0,
  RESOURCE_ID_IMAGE_DAY_1,
  RESOURCE_ID_IMAGE_DAY_2,
  RESOURCE_ID_IMAGE_DAY_3,
  RESOURCE_ID_IMAGE_DAY_4,
  RESOURCE_ID_IMAGE_DAY_5,
  RESOURCE_ID_IMAGE_DAY_6
};

void SetDateImage(SWDATE* SWDate, Layer* layer)
{	
	if (SWDate->State.bInitialized)
	{
		if (SWDate->State.bUpdateDWDigit)
		{
			if (!SWDate->State.bExistsDWImg && !SWDate->State.bSetDWImg && 
				!SWDate->State.bExistsDWLyr && !SWDate->State.bSetDWLyr)
			{
				SWDate->imgDateWord = gbitmap_create_with_resource(DATE_WORD_IMAGES[SWDate->iCurrentDateWordDigit]);
				SWDate->State.bExistsDWImg = true;
				SWDate->bmplyrDateWord = bitmap_layer_create(SWDate->fraDateWordFrame);
				SWDate->State.bExistsDWLyr = true;
				bitmap_layer_set_bitmap(SWDate->bmplyrDateWord, SWDate->imgDateWord); //or .layer?
				SWDate->State.bSetDWImg = true;
				bitmap_layer_set_background_color(SWDate->bmplyrDateWord,GColorClear);
				bitmap_layer_set_compositing_mode(SWDate->bmplyrDateWord, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWDate->bmplyrDateWord));
				SWDate->State.bSetDWLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWDate->bmplyrDateWord));
			}
		}
		//Black 1
		if (SWDate->State.bUpdateMTDigit)
		{
			if (!SWDate->State.bExistsMTImg && !SWDate->State.bSetMTImg && 
				!SWDate->State.bExistsMTLyr && !SWDate->State.bSetMTLyr)
			{
				SWDate->imgDate[0] = gbitmap_create_with_resource(DATE_IMAGES[SWDate->iCurrentMonthTensDigit]);
				SWDate->State.bExistsMTImg = true;
				SWDate->bmplyrDateMonthTens = bitmap_layer_create(SWDate->fraDateMonthTensFrame);
				SWDate->State.bExistsMTLyr = true;
				bitmap_layer_set_bitmap(SWDate->bmplyrDateMonthTens, SWDate->imgDate[0]); //or .layer?
				SWDate->State.bSetMTImg = true;
				bitmap_layer_set_background_color(SWDate->bmplyrDateMonthTens,GColorClear);
				bitmap_layer_set_compositing_mode(SWDate->bmplyrDateMonthTens, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWDate->bmplyrDateMonthTens));
				SWDate->State.bSetMTLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWDate->bmplyrDateMonthTens));
			}
		}
		//Black 0
		if (SWDate->State.bUpdateMODigit)
		{	
			if (!SWDate->State.bExistsMOImg && !SWDate->State.bSetMOImg && 
				!SWDate->State.bExistsMOLyr && !SWDate->State.bSetMOLyr)
			{
				SWDate->imgDate[1] = gbitmap_create_with_resource(DATE_IMAGES[SWDate->iCurrentMonthOnesDigit]);
				SWDate->State.bExistsMOImg = true;
				SWDate->bmplyrDateMonthOnes = bitmap_layer_create(SWDate->fraDateMonthOnesFrame);
				SWDate->State.bExistsMOLyr = true;
				bitmap_layer_set_bitmap(SWDate->bmplyrDateMonthOnes, SWDate->imgDate[1]); //or .layer?
				SWDate->State.bSetMOImg = true;
				bitmap_layer_set_background_color(SWDate->bmplyrDateMonthOnes,GColorClear);
				bitmap_layer_set_compositing_mode(SWDate->bmplyrDateMonthOnes, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWDate->bmplyrDateMonthOnes));
				SWDate->State.bSetMOLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWDate->bmplyrDateMonthOnes));
			}
		}
		//Black 3
		if (SWDate->State.bUpdateDTDigit)
		{
			if (!SWDate->State.bExistsDTImg && !SWDate->State.bSetDTImg && 
				!SWDate->State.bExistsDTLyr && !SWDate->State.bSetDTLyr)
			{
				//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Image  BITMAP 3"); 
				SWDate->imgDate[2] = gbitmap_create_with_resource(DATE_IMAGES[SWDate->iCurrentDayTensDigit]);
				SWDate->State.bExistsDTImg = true;
				SWDate->bmplyrDateDayTens = bitmap_layer_create(SWDate->fraDateDayTensFrame);
				SWDate->State.bExistsDTLyr = true;
				bitmap_layer_set_bitmap(SWDate->bmplyrDateDayTens, SWDate->imgDate[2]); //or .layer?
				SWDate->State.bSetDTImg = true;
				bitmap_layer_set_background_color(SWDate->bmplyrDateDayTens,GColorClear);
				bitmap_layer_set_compositing_mode(SWDate->bmplyrDateDayTens, GCompOpAssign);
				layer_add_child(layer, bitmap_layer_get_layer(SWDate->bmplyrDateDayTens));
				SWDate->State.bSetDTLyr = true;
				layer_mark_dirty(bitmap_layer_get_layer(SWDate->bmplyrDateDayTens));
			}
		}
		//Black 4
		if (SWDate->State.bUpdateDODigit)
		{
		  if (!SWDate->State.bExistsDOImg && !SWDate->State.bSetDOImg && 
			  !SWDate->State.bExistsDOLyr && !SWDate->State.bSetDOLyr)
		  {	
			  //APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Set Image  BITMAP 4"); 
			  SWDate->imgDate[3] = gbitmap_create_with_resource(DATE_IMAGES[SWDate->iCurrentDayOnesDigit]);
			  SWDate->State.bExistsDOImg = true;
			  SWDate->bmplyrDateDayOnes = bitmap_layer_create(SWDate->fraDateDayOnesFrame);
			  SWDate->State.bExistsDOLyr = true;
			  bitmap_layer_set_bitmap(SWDate->bmplyrDateDayOnes, SWDate->imgDate[3]); //or .layer?
			  SWDate->State.bSetDOImg = true;
			  bitmap_layer_set_background_color(SWDate->bmplyrDateDayOnes,GColorClear);
			  bitmap_layer_set_compositing_mode(SWDate->bmplyrDateDayOnes, GCompOpAssign);
			  layer_add_child(layer, bitmap_layer_get_layer(SWDate->bmplyrDateDayOnes));
			  SWDate->State.bSetDOLyr = true;
			  //SWDate->State.bSetDOImg = true;
			  layer_mark_dirty(bitmap_layer_get_layer(SWDate->bmplyrDateDayOnes));	 
		  }
		}
	}
}
			
void SetDateImages(SWDATE* SWDate, Layer* layer)
{		
	SetDateImage(SWDate, layer);
	//SetDateWordImage(SWDate, layer);
	
	SWDate->State.bUpdateDWDigit = false;
	SWDate->State.bUpdateMTDigit = false;
	SWDate->State.bUpdateMODigit = false;
	SWDate->State.bUpdateDTDigit = false;
	SWDate->State.bUpdateDODigit = false;
	//SWDate->iPreviousMonth = SWDate->iCurrentMonth;
}	

void RemoveAndDeIntDate(SWDATE* SWDate)
{
	if (SWDate->State.bInitialized)
	{	
		if (SWDate->State.bExistsDWLyr && SWDate->State.bSetDWLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWDate->bmplyrDateWord));
			bitmap_layer_destroy(SWDate->bmplyrDateWord);
			SWDate->State.bExistsDWLyr = false;
			SWDate->State.bSetDWLyr = false;
		}	
		
		if (SWDate->State.bSetDWImg  && SWDate->State.bExistsDWImg)
		{
			gbitmap_destroy(SWDate->imgDateWord);
			SWDate->State.bSetDWImg = false;
			SWDate->State.bExistsDWImg = false;
		}
		
		if (SWDate->State.bExistsMTLyr && SWDate->State.bSetMTLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWDate->bmplyrDateMonthTens));
			bitmap_layer_destroy(SWDate->bmplyrDateMonthTens);
			SWDate->State.bSetMTLyr = false;
			SWDate->State.bExistsMTLyr = false;
		}
		
		if (SWDate->State.bSetMTImg  && SWDate->State.bExistsMTImg)
		{ 
			gbitmap_destroy(SWDate->imgDate[0]);
			SWDate->State.bSetMTImg = false;
			SWDate->State.bExistsMTImg = false;
		}	
		
		if (SWDate->State.bExistsMOLyr && SWDate->State.bSetMOLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWDate->bmplyrDateMonthOnes));
			bitmap_layer_destroy(SWDate->bmplyrDateMonthOnes);
			SWDate->State.bSetMOLyr = false;
			SWDate->State.bExistsMOLyr = false;
		}
		
		if (SWDate->State.bSetMOImg  && SWDate->State.bExistsMOImg)
		{ 
			gbitmap_destroy(SWDate->imgDate[1]);
			SWDate->State.bSetMOImg = false;
			SWDate->State.bExistsMOImg = false;
		}
		
		if (SWDate->State.bExistsDTLyr && SWDate->State.bSetDTLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWDate->bmplyrDateDayTens));
			bitmap_layer_destroy(SWDate->bmplyrDateDayTens);
			SWDate->State.bSetDTLyr = false;
			SWDate->State.bExistsDTLyr = false;
		}
		
		if (SWDate->State.bSetDTImg  && SWDate->State.bExistsDTImg)
		{ 
			gbitmap_destroy(SWDate->imgDate[2]);
			SWDate->State.bSetDTImg = false;
			SWDate->State.bExistsDTImg = false;
		}

		if (SWDate->State.bExistsDOLyr && SWDate->State.bSetDOLyr)
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWDate->bmplyrDateDayOnes));
			bitmap_layer_destroy(SWDate->bmplyrDateDayOnes);
			SWDate->State.bSetDOLyr = false;
			SWDate->State.bExistsDOLyr = false;
		}

		if (SWDate->State.bSetDOImg  && SWDate->State.bExistsDOImg)
		{ 
			gbitmap_destroy(SWDate->imgDate[3]);
			SWDate->State.bSetDOImg = false;
			SWDate->State.bExistsDOImg = false;
		}		
	}
}

void SetCurrentDate(SWDATE* SWDate, Layer* layer)
{
	if (SWDate->State.bInitialized)
	{
		//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Remove and deint prev."); 
		RemoveAndDeIntDate(SWDate);
		//SWDate->iCurrentMonth = t->tm_hour;
		SetDateImages(SWDate, layer);
	}
}

void GetCurrentDate(SWDATE* SWDate)
{
	time_t tDate = time(NULL);
	struct tm* tmDate = localtime(&tDate);
	int month = tmDate->tm_mon;
	SWDate->iCurrentMonth = month +1;	
	SWDate->iCurrentDay = tmDate->tm_mday;	
	SWDate->iCurrentDateWordDigit = tmDate->tm_wday;
	
	SWDate->iCurrentMonthTensDigit = SWDate->iCurrentMonth / 10;
	SWDate->iCurrentMonthOnesDigit = SWDate->iCurrentMonth % 10;
	
	SWDate->iCurrentDayTensDigit = SWDate->iCurrentDay / 10;
	SWDate->iCurrentDayOnesDigit = SWDate->iCurrentDay % 10;
	
	/*if (SWDate->iPreviousDateWordDigit != SWDate->iCurrentDateWordDigit)
	{
		SWDate->State.bUpdateDWDigit = true;
	}
	else
	{
		SWDate->State.bUpdateDWDigit = false;
	}
	SWDate->iPreviousDateWordDigit = SWDate->iCurrentDateWordDigit;
	
	if (SWDate->iPreviousMonthTensDigit != SWDate->iCurrentMonthTensDigit)
	{
		SWDate->State.bUpdateMTDigit = true;
	}
	else
	{
		SWDate->State.bUpdateMTDigit = false;
	}
	SWDate->iPreviousMonthTensDigit = SWDate->iCurrentMonthTensDigit;
	
	if (SWDate->iPreviousMonthOnesDigit != SWDate->iCurrentMonthOnesDigit)
	{
		SWDate->State.bUpdateMODigit = true;
	}
	else
	{
		SWDate->State.bUpdateMODigit = false;
	}
	SWDate->iPreviousMonthOnesDigit = SWDate->iCurrentMonthOnesDigit;
	
	if (SWDate->iPreviousDayTensDigit != SWDate->iCurrentDayTensDigit)
	{
		SWDate->State.bUpdateDTDigit = true;
	}
	else
	{
		SWDate->State.bUpdateDTDigit = false;
	}
	SWDate->iPreviousDayTensDigit = SWDate->iCurrentDayTensDigit;
	
	//APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "prev min digit: %d current min ones digit: %d",SWDate->iPreviousDayOnesDigit,SWDate->iCurrentDayOnesDigit); 
	if (SWDate->iPreviousDayOnesDigit != SWDate->iCurrentDayOnesDigit)
	{
		SWDate->State.bUpdateDODigit = true;
	}
	else
	{
		SWDate->State.bUpdateDODigit = false;
	}
	SWDate->iPreviousDayOnesDigit = SWDate->iCurrentDayOnesDigit;
	*/
	
	SWDate->State.bUpdateDWDigit = true;
	SWDate->State.bUpdateMTDigit = true;
	SWDate->State.bUpdateMODigit = true;
	SWDate->State.bUpdateDTDigit = true;
	SWDate->State.bUpdateDODigit = true;
	
}

void SetCurrentDateAndDateWord(SWDATE* SWDate, Layer* layer)
{
	////APP_LOG(APP_LOG_LEVEL_DEBUG_VERBOSE, "Setting Current Date and Word");
	GetCurrentDate(SWDate);
	SetCurrentDate(SWDate, layer);
	//SetCurrentDateWord(SWDate, layer);
}

void SWDATE_INIT(SWDATE* SWDate)
{
	if (!SWDate->State.bInitialized)
	{
		//DATE_IMAGES
		SWDate->State.bInitialized = true;
		SWDate->fraDateFrame = WATCH_FRA_DATE;
		SWDate->fraDateWordFrame = WATCH_FRA_DATE_WORD;
		SWDate->fraDateMonthTensFrame = WATCH_FRA_DATE_MONTH_TENS;
		SWDate->fraDateMonthOnesFrame = WATCH_FRA_DATE_MONTH_ONES;
		SWDate->fraDateDayTensFrame = WATCH_FRA_DATE_DAY_TENS;
		SWDate->fraDateDayOnesFrame = WATCH_FRA_DATE_DAY_ONES;
		
		SWDate->iCurrentDateWordDigit = 0;
		SWDate->iCurrentMonthTensDigit = 99;
		SWDate->iCurrentMonthOnesDigit = 99;
		SWDate->iCurrentDayTensDigit = 99;
		SWDate->iCurrentDayOnesDigit = 99;
		
		SWDate->iPreviousDateWordDigit = 88;
		SWDate->iPreviousMonthTensDigit = 88;
		SWDate->iPreviousMonthOnesDigit = 88;
		SWDate->iPreviousDayTensDigit = 88;
		SWDate->iPreviousDayOnesDigit = 88;
		
		SWDate->State.bUpdateDWDigit = true;
		SWDate->State.bUpdateMTDigit = true;
		SWDate->State.bUpdateMODigit = true;
		SWDate->State.bUpdateDTDigit = true;
		SWDate->State.bUpdateDODigit = true;
	}
}

void SWDATE_DEINIT(SWDATE* SWDate)
{
	SWDate->State.bInitialized = false;	
}
